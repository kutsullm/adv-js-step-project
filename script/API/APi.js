//token a308511d-5626-4faf-8167-68a53cb46e93
class API {
  constructor() {
    this.baseUrl = "https://ajax.test-danit.com/api/v2"
    this.token = this.getToken() || null
  }
  async getData(url) {
    const res = await fetch(`${this.baseUrl}${url}`, {
      method: "GET",
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${this.token}`
      },
    })
    if (res.ok) {
      return res.json()
    }
  }
  async createData(url, data) {
    console.log(this.token)
    const res = await fetch("https://ajax.test-danit.com/api/v2/cards", {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${this.token}`
      },
      body: JSON.stringify({
        title: 'Визит к кардиологу',
        description: 'Плановый визит',
        doctor: 'Cardiologist',
        bp: '24',
        age: 23,
        weight: 70
      })
  
    })
    console.log(data)
    if (res.ok) {
      return res.json()
    }
  }
  async deleteData(url, id) {
    const res = await fetch(`${this.baseUrl}${url}/${id}`, {
      method: "DELETE",
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${this.token}`
      }

    })
    if (res.ok) {
      return res
    }

  }
  async editData(url, id, data) {
    const res = await fetch(`${this.baseUrl}${url}/${id}`, {
      method: "PUT",
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${this.token}`
      },
      body: JSON.stringify(data)
    })
    if (res.ok) {
      return res.json()
    }
  }
  async getDataById(url, id) {
    const res = await fetch(`${this.baseUrl}${url}/${id}`,{
      headers: {
        'Authorization': `Bearer ${this.token}`
      }
    })
    if (res.ok) {
      return res.json()
    }
  }

  getToken() {
    const token = localStorage.getItem("token")
    if (token) {
      this.token = token

    }
    return this.token
  }
  async getDataToken(url,data) {
    const res = await fetch(`${this.baseUrl}${url}`,{
      method:"POST",
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(data)
    })
    if (res.ok) {
      const token = await res.text()
      localStorage.setItem("token", token)
      this.token = token
      return this.token
    }


  }
}

export default new API()